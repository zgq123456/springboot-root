package com.young.app.api;

import com.young.app.config.FeignSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author ：young
 * @date ：Created in 2019/7/6
 * @description：somting SpringBoot2.0之后，Feigin的value不能再重复了
 * 读完开发文档，认为Value取值为服务名-接口名的
 * 格式来命名比较好。
 */
@FeignClient(value = "app",configuration = FeignSupportConfig.class)
@RequestMapping(value = "/app")
public interface FeignDemo {
    @RequestMapping(value = "/feignDemo", method = RequestMethod.GET)
    String feignDemo();
}
