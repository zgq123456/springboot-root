package com.young.app.common;

/**
 * @author ：young
 * @date ：Created in 2019/7/14
 * @description：返回结果类
 */
public class BaseResult<S> {
    int code;

    S message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public S getMessage() {
        return message;
    }

    public void setMessage(S message) {
        this.message = message;
    }

}
