package com.young.app.hystrix;

import com.young.app.api.FeignDemo;
import com.young.app.common.BaseResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author ：young
 * @date ：Created in 2019/7/14
 * @description：熔断
 */
@Component
public class ServerFeignClientFallBackFactory implements FallbackFactory<FeignDemo> {

    @Override
    public FeignDemo create(Throwable throwable) {
        return new FeignDemo() {
            @Override
            public String feignDemo() {
                BaseResult<String> baseResult = new BaseResult<>();
                baseResult.setCode(-1);
                baseResult.setMessage("fail");
                return baseResult.getMessage();
            }
        };
    }
}
