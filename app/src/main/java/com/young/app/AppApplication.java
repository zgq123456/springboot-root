package com.young.app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableDiscoveryClient
@EnableEurekaServer
@SpringBootApplication
public class AppApplication {
    private static Logger logger = LogManager.getLogger(AppApplication.class);

    public static void main(String[] args) {
        logger.info("项目启动*****************************");
        SpringApplication.run(AppApplication.class, args);
    }
}
