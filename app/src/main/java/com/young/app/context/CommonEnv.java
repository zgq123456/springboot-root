package com.young.app.context;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author ：young
 * @date ：Created in 2019/7/24
 * @description：环境准备
 * CommandLineRunner接口的实现类会在所有Spring Beans初始化
 * 完成之后，SpringApplication.run()执行之前执行run()
 */
@Component
public class CommonEnv implements CommandLineRunner {
    private static Logger logger = LogManager.getLogger(CommonEnv.class);

    @Override
    public void run(String... args) throws Exception {
        logger.info("加载初始化数据");
    }
}
