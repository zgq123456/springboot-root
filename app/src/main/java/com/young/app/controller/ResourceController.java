package com.young.app.controller;

import com.young.app.service.LoadResourceService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 程序运行时配置文件读取
 */
@RestController
@RequestMapping(value = "/app")
public class ResourceController {
    @Resource
    LoadResourceService loadResourceService;

    /**
     *
     * @throws IOException
     */
    @RequestMapping(value = "/loadResource", method = RequestMethod.GET)
    String loadResourceService() throws IOException {
        loadResourceService.getResourceFile("static","text.txt");
        loadResourceService.getResourceFile("static","excel.xlsx");
        return "success";
    }

    /**
     *
     * @throws IOException
     */
    @RequestMapping(value = "/loadResource2", method = RequestMethod.GET)
    void loadResourceService2() throws IOException {
        loadResourceService.getResourceFile2("static","text.txt");
        loadResourceService.getResourceFile2("static","excel.xlsx");
    }
    /**
     *
     * @throws IOException
     */
    @RequestMapping(value = "/loadResource3", method = RequestMethod.GET)
    void loadResourceService3() throws IOException {
        loadResourceService.getResourceFile3("static","text.txt");
        loadResourceService.getResourceFile3("static","excel.xlsx");
    }
    /**
     *
     * @throws IOException
     */
    @RequestMapping(value = "/loadResource4", method = RequestMethod.GET)
    void loadResourceService4() throws IOException {
        loadResourceService.getResourceFile4("static","text.txt");
        loadResourceService.getResourceFile4("static","excel.xlsx");
    }
}
