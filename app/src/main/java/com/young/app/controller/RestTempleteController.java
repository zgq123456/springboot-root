package com.young.app.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：young
 * @date ：Created in 2019/7/6 21:15
 * @description：somting
 */
@RestController
@RequestMapping(value = "/app")
public class RestTempleteController {
    private static Logger logger = LogManager.getLogger(WebClientController.class);

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String helloConsumer() {
        return "hello";
    }
}
