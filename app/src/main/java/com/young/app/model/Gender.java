package com.young.app.model;

/**
 * 性别枚举
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/8/10
 * @description：性别
 */
public enum Gender {
    MALE("0", "男"),
    FEMALE("1", "女");

    Gender(String s, String s1) {
    }
}
