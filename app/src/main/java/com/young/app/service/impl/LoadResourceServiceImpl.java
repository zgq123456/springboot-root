package com.young.app.service.impl;

import com.young.app.service.LoadResourceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.*;

@Service("loadResourceService")
public class LoadResourceServiceImpl implements LoadResourceService {
    private final Logger logger = LogManager.getLogger(LoadResourceServiceImpl.class);

    /**
     * Local无效
     * jar环境无效（winsows）
     * @param dir
     * @param name
     * @throws IOException
     */
    @Override
    public void getResourceFile(String dir, String name) throws IOException {
        String filePath = Thread.currentThread().getContextClassLoader().getResource(dir + "/" + name).getFile();
        File file = new File(filePath);
        logger.info("资源路径：{}", filePath);
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] cache = new byte[1024 * 1024];
        int len = fileInputStream.read(cache);
        File outFile = new File("E://file//" + name);
        FileOutputStream out = new FileOutputStream(outFile);
        out.write(cache, 0, len);
        out.close();
    }

    /**
     * local无效
     * jar有效
     * @param dir
     * @param name
     * @throws IOException
     */
    @Override
    public void getResourceFile2(String dir, String name) throws IOException {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(dir + "/" + name);
        logger.info("资源路径：{}");
        byte[] cache = new byte[1024 * 1024];
        int len = is.read(cache);
        File outFile = new File("E://file//" + name);
        FileOutputStream out = new FileOutputStream(outFile);
        out.write(cache, 0, len);
        out.close();
    }
    /**
     * Local无效
     * jar有效
     * @param dir
     * @param name
     * @throws IOException
     */
    @Override
    public void getResourceFile3(String dir, String name) throws IOException {
        InputStream is = this.getClass().getResourceAsStream("/" + dir + "/" + name);
        logger.info("资源路径：{}");
        byte[] cache = new byte[1024 * 1024];
        int len = is.read(cache);
        File outFile = new File("E://file//" + name);
        FileOutputStream out = new FileOutputStream(outFile);
        out.write(cache, 0, len);
        out.close();
    }
    /**
     * Local无效
     * jar环境有效(winsows）
     * @param dir
     * @param name
     * @throws IOException
     */
    @Override
    public void getResourceFile4(String dir, String name) throws IOException {
        byte[] cache = new byte[1024 * 1024];
        int len = 0;
        Resource res = new ClassPathResource(dir + "/" + name);
        logger.info("资源路径：{}", ((ClassPathResource) res).getPath());
        InputStream is = res.getInputStream();
        len = is.read(cache);
        if (len != 1) {
            File outFile = new File("E://file//" + name);
            FileOutputStream out = new FileOutputStream(outFile);
            out.write(cache, 0, len);
            out.close();
        }
    }

}
