package com.young.app.listener;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.sun.javafx.binding.StringFormatter;
import com.young.app.TestUtils;
import com.young.app.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class UserExcelListenerTest {
    private static Logger logger = LogManager.getLogger(UserExcelListenerTest.class);

    /**
     * 非Web环境读取Excel
     */
    @Test
    public void readExcel() {
        String fileName = TestUtils.getPath() + "excel" + File.separator + "UserList.xlsx";
        EasyExcel.read(fileName, User.class, new SimpleExcelListener()).sheet().doRead();
    }

    @Test
    public void writeExcel() {
        String dir = TestUtils.getPath() + "write";
        createDir(dir);
        String fileName = dir + File.separator + System.currentTimeMillis() + ExcelTypeEnum.XLS.getValue();
        // write有重载方法，可以输出到OutputStream
        EasyExcel.write(fileName, User.class)
                .excelType(ExcelTypeEnum.XLS)
                .sheet("模板")
                .doWrite(data());
        logger.info(StringFormatter.format("数据输出到文件[%s]", fileName).getValueSafe());
    }

    private List data() {
        List<User> list = new ArrayList<User>();
        for (int i = 0; i < 10; i++) {
            User tempUser = new User();
            tempUser.setName("姓名" + i);
            tempUser.setAge(20 + i);
            tempUser.setBirth(new Date().toString());
            list.add(tempUser);
        }
        return list;
    }

    /**
     * 创建目录
     *
     * @param destDirName
     * @return
     */
    public boolean createDir(String destDirName) {
        File dir = new File(destDirName);
        if (dir.exists()) {
            System.out.println("目标目录已经存在");
            return false;
        }
        if (!destDirName.endsWith(File.separator)) {
            destDirName = destDirName + File.separator;
        }
        //创建目录
        if (dir.mkdirs()) {
            System.out.println("创建目录" + destDirName + "成功！");
            return true;
        } else {
            System.out.println("创建目录" + destDirName + "失败！");
            return false;
        }
    }
}