package com.young.common.frame;

/**
 * 响应码枚举
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/3/28
 */
public enum ResultCode {
    //成功
    SUCCESS("200"),
    //失败
    FAIL("400"),
    //未认证（签名错误）
    UNAUTHORIZED("401"),
    //接口不存在
    NOT_FOUND("404"),
    //参数异常
    PARAM_ILLEGAL("405"),
    //服务器内部错误
    INTERNAL_SERVER_ERROR("500");

    private final String code;

    ResultCode(String code) {
        this.code = code;
    }

    public String code() {
        return code;
    }
}