package com.young.common.utils;

/**
 * 时间控制类型工具
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/3/29
 */
public class TimeUtils {
    /**
     * 延时工具
     *
     * @param millis
     */
    public static void sleep(long millis) {
        while (millis > 0) {
            long begin = System.currentTimeMillis();
            try {
                Thread.sleep(millis);
            } catch (Exception e) {
            }
            millis -= System.currentTimeMillis() - begin;
        }
    }
}
