package com.young.consul;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Consul注册DEMO
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/11/29
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ConsulApplication {
    private static Logger logger = LogManager.getLogger(ConsulApplication.class);

    public static void main(String[] args) {
        logger.info("项目启动*****************************");
        SpringApplication.run(ConsulApplication.class, args);
    }
}
