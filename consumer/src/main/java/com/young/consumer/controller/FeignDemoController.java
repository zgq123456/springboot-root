package com.young.consumer.controller;

import com.young.app.api.FeignDemo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：young
 * @date ：Created in 2019/7/6
 * @description：somting
 */
@RestController
@RequestMapping(value = "/consumer")
public class FeignDemoController {
    private static Logger logger = LogManager.getLogger(FeignDemoController.class);

    @Autowired
    FeignDemo feignDemo;

    @RequestMapping(value = "feginDemo", method = RequestMethod.GET)
    public String feginDemo() {
        return feignDemo.feignDemo();
    }
}
