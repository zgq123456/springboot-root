package com.young.consumer.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.http.client.reactive.ClientHttpRequest;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * @author ：young
 * @date ：Created in 2019/7/8
 * @description：WebClient
 */
@RestController
@RequestMapping(value = "consumer")
public class WebClientController {
    private static Logger logger = LogManager.getLogger(WebClientController.class);

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/webClientGet", method = RequestMethod.GET)
    public Mono<String> webClientGet() {
        WebClient webClient = WebClient.builder()
                .baseUrl("http://127.0.0.1:9528")
                .build();
        return webClient.get()
                .uri("/app/webClientGet")
                .retrieve()
                .bodyToMono(String.class);

    }

    @RequestMapping(value = "/webClientPost", method = RequestMethod.POST)
    public Mono<String> webClientPost() {
        WebClient webClient = WebClient.builder()
                .baseUrl("http://127.0.0.1:9528")
                .build();
        return webClient.post()
                .uri("/app/webClientPost")
                .retrieve()
                .bodyToMono(String.class);

    }

    @RequestMapping(value = "/webClientMutilpartFile", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Mono<String> webClientMutilpartFile(@RequestParam("file") MultipartFile file) {
        /**
         * 这段代码还没有调通
         */
        logger.info("uploadFile filename={}", file.getOriginalFilename());
        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("file", file);
        MultiValueMap<String, HttpEntity<?>> multiValueMap = builder.build();
        WebClient webClient = WebClient.builder()
                .baseUrl("http://127.0.0.1:9528")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA_VALUE)
                .build();
        return webClient.post()
                .uri("/app/webClientMutilpartFile")
                .body((BodyInserter<?, ? super ClientHttpRequest>) multiValueMap)
                .retrieve()
                .bodyToMono(String.class);

    }
}
