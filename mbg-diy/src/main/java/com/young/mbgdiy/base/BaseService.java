package com.young.mbgdiy.base;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * TODO
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/4/5
 */
public interface BaseService<T, Example extends BaseExample, ID> {

    long countByExample(Example example);

    int deleteByExample(Example example);

    int deleteByPrimaryKey(ID id);

    int insert(T record);

    int insertSelective(T record);

    List<T> selectByExample(Example example);

    /**
     * return T object
     *
     * @param example
     * @return
     * @author Marvis
     * @date May 23, 2018 11:37:11 AM
     */
    T selectByCondition(Example example);


    void selectByPageExample(Example example, PageInfo pageInfo);

    /**
     * diy 分页查询
     *
     * @param example
     * @param currentPage
     * @param pageSize
     * @return
     */
    PageInfo<T> selectByPageExample(Example example, Integer currentPage, Integer pageSize);

    T selectByPrimaryKey(ID id);

    int updateByExampleSelective(@Param("record") T record, @Param("example") Example example);

    int updateByExample(@Param("record") T record, @Param("example") Example example);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);
}