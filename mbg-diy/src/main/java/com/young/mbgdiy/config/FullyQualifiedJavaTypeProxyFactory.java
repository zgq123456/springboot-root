package com.young.mbgdiy.config;

import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;

/**
 * TODO
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/4/5
 */
public class FullyQualifiedJavaTypeProxyFactory extends FullyQualifiedJavaType {

    private static FullyQualifiedJavaType pageInfoInstance = new FullyQualifiedJavaType("com.young.mbgdiy.base.PageInfo");
    private static FullyQualifiedJavaType baseExampleInstance = new FullyQualifiedJavaType("com.young.mbgdiy.base.BaseExample");
    private static FullyQualifiedJavaType baseMapperInstance = new FullyQualifiedJavaType("com.young.mbgdiy.base.BaseMapper");
    private static FullyQualifiedJavaType baseServiceInstance = new FullyQualifiedJavaType("com.young.mbgdiy.base.BaseService");
    private static FullyQualifiedJavaType baseServiceImplInstance = new FullyQualifiedJavaType("com.young.mbgdiy.base.BaseServiceImpl");

    public FullyQualifiedJavaTypeProxyFactory(String fullTypeSpecification) {
        super(fullTypeSpecification);
    }

    public static final FullyQualifiedJavaType getPageInfoInstanceInstance() {

        return pageInfoInstance;
    }

    public static final FullyQualifiedJavaType getBaseExampleInstance() {

        return baseExampleInstance;
    }

    public static final FullyQualifiedJavaType getBaseMapperInstance() {

        return baseMapperInstance;
    }

    public static final FullyQualifiedJavaType getBaseServiceInstance() {

        return baseServiceInstance;
    }

    public static final FullyQualifiedJavaType getBaseServiceImplInstance() {

        return baseServiceImplInstance;
    }
}
