package com.young.mbgdiy.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 当前日期获取
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/4/6
 */
public class CurrentDateUtil {
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    public static String getDate() {
        return dateFormatter.format(new Date());
    }
}
