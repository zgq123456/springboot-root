package com.young.mybatisdiy.mapper;

import com.young.mbgdiy.base.BaseMapper;
import com.young.mybatisdiy.model.MbgDiy;
import com.young.mybatisdiy.model.MbgDiyExample;
import org.springframework.stereotype.Repository;

/**
  *@author young
  *@date   2020-04-14
  */
@Repository
public interface MbgDiyMapper extends BaseMapper<MbgDiy, MbgDiyExample, String> {
}