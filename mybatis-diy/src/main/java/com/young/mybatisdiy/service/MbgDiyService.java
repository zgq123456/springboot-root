/*** copyright (c) 2020 young  ***/
package com.young.mybatisdiy.service;

import com.young.mbgdiy.base.BaseService;
import com.young.mybatisdiy.model.MbgDiy;
import com.young.mybatisdiy.model.MbgDiyExample;

public interface MbgDiyService extends BaseService<MbgDiy, MbgDiyExample, String> {
}