package run;

import com.young.mbgdiy.config.ConfigurationParserOverride;
import com.young.mybatisdiy.mapper.MbgDiyMapper;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * DIY代码生成工具
 * {@link MbgDiyMapper} 有添加注解、注释的操作空间
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/4/5
 */
public class Generator {

    public void generator() throws Exception {

        List<String> warnings = new ArrayList<String>();
        boolean overwrite = true;
        /**
         * 这个路径需要注意
         * 如果是一级项目，直接：src/main/resources/generatorConfig.xml
         * 如果是有父级项目，需要包含子项目名，mybatis-diy/src/main/resources/generatorConfig.xml
         */
        File configFile = new File("mybatis-diy/src/main/resources/generatorConfig.xml");

        ConfigurationParserOverride cp = new ConfigurationParserOverride(warnings);
        Configuration config = cp.parseConfiguration(configFile);
        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);

        myBatisGenerator.generate(null);
        warnings.stream().forEach(d -> System.out.println(d));

    }

    public static void main(String[] args) throws Exception {
        try {
            Generator generator = new Generator();
            generator.generator();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
