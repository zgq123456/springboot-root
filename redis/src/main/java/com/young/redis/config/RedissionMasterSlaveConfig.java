package com.young.redis.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.ReadMode;
import org.redisson.config.SslProvider;
import org.redisson.config.SubscriptionMode;

import java.net.URI;
import java.net.URISyntaxException;


/**
 * TODO
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/11/23
 */
public class RedissionMasterSlaveConfig {
    public RedissonClient masterSlave() throws URISyntaxException {
        Config config = new Config();
        config.useMasterSlaveServers()
                //可以用"rediss://"来启用SSL连接
                .setMasterAddress("redis://127.0.0.1:6379")
                .addSlaveAddress("redis://127.0.0.1:6389", "redis://127.0.0.1:6332", "redis://127.0.0.1:6419")
                .addSlaveAddress("redis://127.0.0.1:6399")
                .setDnsMonitoringInterval(5000) //DNS监控间隔，单位：毫秒
                .setReadMode(ReadMode.MASTER_SLAVE) //读取操作的负载均衡模式
                .setSubscriptionMode(SubscriptionMode.SLAVE) //订阅操作的负载均衡模式,SLAVE - 只在从服务节点里订阅。 MASTER - 只在主服务节点里订阅
                .setLoadBalancer(new org.redisson.connection.balancer.RoundRobinLoadBalancer()) // 负载均衡算法类的选择
                .setSubscriptionConnectionMinimumIdleSize(1) //从节点发布和订阅连接的最小空闲连接数
                .setSubscriptionConnectionPoolSize(50) //从节点发布和订阅连接池大小
                .setSlaveConnectionMinimumIdleSize(32) //从节点最小空闲连接数
                .setSlaveConnectionPoolSize(64) //从节点连接池大小
                .setMasterConnectionMinimumIdleSize(32) //主节点最小空闲连接数
                .setMasterConnectionPoolSize(64) //主节点连接池大小
                .setIdleConnectionTimeout(10000) //连接空闲超时，单位：毫秒
                .setConnectTimeout(10000) //连接超时，单位：毫秒
                .setTimeout(3000) //命令等待超时，单位：毫秒
                .setRetryAttempts(3) //命令(发送)失败重试次数
                .setRetryInterval(1500) // 命令重试发送时间间隔，单位：毫秒,与retryAttempts配合，三次重试均失败，则对应节点需要被剔除
                .setDatabase(0) // 数据库编号
                //.setPassword("") //密码设置
                .setSubscriptionsPerConnection(5) //单个连接最大订阅数量
                .setSslEnableEndpointIdentification(true) //启用SSL终端识别
                .setSslProvider(SslProvider.JDK) // SSL实现方式
                //.setSslTruststorePassword("") //SSL信任证书库密码
                //.setSslKeystore(new URI("")) //SSL钥匙库路径
                //.setSslKeystorePassword("")//SSL钥匙库密码
                .setClientName("clientName");
        RedissonClient redisson = Redisson.create(config);
        return redisson;
    }
}
