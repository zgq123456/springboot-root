OpenMessageing，其中包括建立行业准则和消息传递，流式规范，为金融，电子商务，
物联网和大数据领域提供通用框架。设计原则是分布式异构环境中面向云、简单、灵活和
独立语言的设计原则。符合这些规范将使在所有主要平台设操作系统上开发异构消息传递
应用成为可能。