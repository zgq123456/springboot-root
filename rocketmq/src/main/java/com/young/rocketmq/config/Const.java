package com.young.rocketmq.config;

/**
 * RocketMQ Config
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/3/29
 */
public class Const {
    public static final String NAME_SRV = "192.168.23.128:9876";
    public static final String GROUP = "young";
    public static final String TOPIC = "TopicTest";
}
