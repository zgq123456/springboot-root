package com.young.rocketmq.simpleexample.producer;

import com.young.rocketmq.config.Const;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

/**
 * 可靠同步
 * Send Messages Synchronously
 * Reliable synchronous transmission is used in extensive scenes,
 * such as important notification messages, SMS notification,
 * SMS marketing system, etc.
 * 可靠的同步传输被广泛应用于重要的通知消息、短信通知、短信营销系统等场景
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2020/3/29
 */
public class SyncProducer {

    private static Logger logger = LogManager.getLogger(SyncProducer.class);

    public static void main(String[] args) throws Exception {
        //Instantiate with a producer group name.
        DefaultMQProducer producer = new DefaultMQProducer(Const.GROUP);
        // Specify name server addresses.
        producer.setNamesrvAddr(Const.NAME_SRV);
        //Launch the instance.
        producer.start();
        for (int i = 0; i < 2; i++) {
            // Create a message instance, specifying topic, tag and message body.
            Message msg = new Message(Const.TOPIC, "TagA", ("Hello RocketMQ " + i).getBytes(RemotingHelper.DEFAULT_CHARSET));
            // Call send message to deliver message to one of brokers.
            // 调用send方法，将message发送给其中一个broker
            SendResult sendResult = producer.send(msg);
            logger.info(String.format("%s%n", sendResult));
        }
        //Shut down once the producer instance is not longer in use.
        producer.shutdown();
    }
}
