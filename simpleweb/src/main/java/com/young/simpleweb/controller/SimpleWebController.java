package com.young.simpleweb.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author ：young
 * @date ：Created in 2019/7/9
 * @description：简单请求
 */
@RestController
@RequestMapping(value = "simpleWeb")
public class SimpleWebController {

    @RequestMapping(value = "simplePost")
    public Map simplePost(HttpServletRequest request, HttpServletResponse response) {
        response.addHeader("method", "simplePost");
        return request.getParameterMap();
    }
}
