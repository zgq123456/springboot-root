package com.young.simpleweb.entity;

import com.young.common.validation.ValidationGroup;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 参数校验测试用例
 *
 * @author ：young
 * @date ：Created in 2020/09/23
 * @description：user entity
 */
public class ParamUser implements Cloneable {
    /**
     * 分组测试
     *
     * @see {@link Validated}
     * @see {@link com.young.simpleweb.controller.ValidatorController#paramCheckInsert}
     * @see {@link com.young.simpleweb.controller.ValidatorController#paramCheckUpdate}
     */
    @NotNull(groups = ValidationGroup.Insert.class)
    @Null(groups = ValidationGroup.Update.class)
    private String name;
    /**
     * hibernate-validator-api
     * 测试
     *
     * @see {@link com.young.simpleweb.controller.ValidatorController#paramCheckValid}
     */
    @Length(min = 1, max = 10, message = "sex can't be null")
    private String sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "ParamUser{" +
                "name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}
