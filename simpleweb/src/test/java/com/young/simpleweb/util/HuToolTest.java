package com.young.simpleweb.util;

import cn.hutool.core.convert.Convert;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.Date;

/**
 * @author ：young
 * @date ：Created in 2019/7/22
 * @description：hutool工具类测试
 */
public class HuToolTest {
    private static Logger logger = LogManager.getLogger(HuToolTest.class);

    @Test
    public void strToDate(){
        String a = "2019-07-22 21:04:39";
        Date value = Convert.toDate(a);
        logger.info("时间：{}",value.toString());
    }
}
