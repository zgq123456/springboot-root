package com.young.swagger2jmeter.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * demo Controller
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2021/3/21
 */
@Api(tags = "swagger api demo")
@RestController
@RequestMapping(value = "swagger-jmeter")
public class SwaggerToJmeterController {
    @ApiOperation(value ="demo接口" )
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "name",value ="参数1" ,required = true,paramType = "query",dataType = "String",defaultValue = "young"),
            @ApiImplicitParam(name = "location",value ="参数2" ,required = true,paramType = "query",dataType = "String",defaultValue = "Beijing"),
    })
    @RequestMapping(value = "getMessage",method = RequestMethod.POST)
    public String getMessage(@RequestParam(name = "name") String name,@RequestParam(name = "location") String location) {
        return "hi,i am " + name + " from  " + location;
    }
}
