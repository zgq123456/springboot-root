package com.young.swagger2jmeter.jmeter;

import java.io.File;

/**
 * JMeter脚本导出工具
 *
 * @author ：<a href="mailto:youngkun2016@163.com">young</a>
 * @date ：Created in 2021/3/20
 */
public class JMeterExporter {
    public Boolean ExportSessions(String filePath, Session[] oSessions) {
        Boolean flag = true;
        filePath = filePath + "JMeterScript.jmx";
        if (filePath == null) {
            return false;
        }
        try {
            new TestPlan(oSessions, filePath).saveAsJMeterScript();
        } catch (Exception exception) {
            System.out.println("导出脚本出错，错误信息如下：");
            exception.printStackTrace();
            flag = false;
        }
        return flag;
    }

    public static void main(String[] args) {
        JMeterExporter exporter = new JMeterExporter();
        String filePath = "." + File.separator;
        String[] paths = new String[]{"helloWorld"};
        Session[] sessions = new Session[paths.length];
        for (int index = 0, len = paths.length; index < len; index++) {
            sessions[index] = new Session();
            sessions[index].setContentType("application/x-www-form-urlencoded");
            sessions[index].setContextType("application/x-www-form-urlencoded");
            sessions[index].setHostname("127.0.0.1");
            sessions[index].setPort("8080");
            sessions[index].setProtocol("http");
            sessions[index].setRequestBody("");
            sessions[index].setRequestMethod("POST");
            sessions[index].setRequestPath(paths[index]);
        }
        exporter.ExportSessions(filePath, sessions);
    }
}