package com.young.zk.client;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.data.Stat;
import org.junit.Assert;
import org.junit.Test;

import java.util.logging.Logger;

public class CuratorClient {

    private static Logger logger = Logger.getLogger(CuratorClient.class.toString());

    /**
     * 创建测试
     *
     * @throws Exception
     */
    @Test
    public void createPathTest() throws Exception {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory.newClient("127.0.0.1:2181", retryPolicy);
        client.start();
        if (null == client.checkExists().forPath("/path")) {
            client.create().forPath("/path");
        }
        Stat stat = client.checkExists().forPath("/path");
        logger.info(String.format("检查结果：{%s}", stat.toString()));
        Assert.assertTrue("失败", stat != null);
        client.close();
    }

}